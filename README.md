# Self-made Tool List

A.k.a. (as somebody may call them) dog food list

## Small-scale tool

### Native application development

* [dependency-checker](https://gitlab.com/chiakikame/rust-dependency-checker)
  can list dependency of a native binary application (executable)

### Web

* [redirection-probe](https://gitlab.com/chiakikame/rust-redirection-probe)
  can tell if there's HTTP(S) 3XX redirection.

## Library / format investigation helper

* [quickxml-events](https://gitlab.com/chiakikame/rust-quickxml-events)
  is for investigating event flow of an XML file processed by quick-xml crate
  in Rust
